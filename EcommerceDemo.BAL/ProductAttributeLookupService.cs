﻿using System;
using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Linq;

namespace EcommerceDemo.BAL
{
    public class ProductAttributeLookupService : IProductAttributeLookupService
    {

        IUnitOfWork Unitofwork;

         public ProductAttributeLookupService(IUnitOfWork unitOfWork)
        {
            Unitofwork = unitOfWork;
        }

        /// <summary>
        /// Add product attribute
        /// </summary>
        /// <param name="entity">product attribute to be added</param>
        public void Add(ProductAttributeLookup entity)
        {
            Unitofwork.ProductAttributeLookupRepository.Add(entity);
            Unitofwork.Commit();
            // _context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Update the product attribute based on id
        /// </summary>
        /// <param name="productAttributeId">product attribute id to be updated</param>
        /// <param name="productAttributeEntity">product attribute information to be  updated</param>
        /// <returns>true/false</returns>
        public bool Update(int productAttributeId, ProductAttributeLookup productAttributeEntity)
        {
            var success = false;
            if (productAttributeEntity != null)
            {
                
                var productAttribute = Unitofwork.ProductAttributeLookupRepository.GetByID(productAttributeId);
                if (productAttributeEntity != null)
                {
                    productAttribute.ProdCatId = productAttributeEntity.ProdCatId;
                    productAttribute.AttributeName = productAttributeEntity.AttributeName;
                 
                    Unitofwork.ProductAttributeLookupRepository.Update(productAttribute);
                    Unitofwork.Commit();                   
                    success = true;
                }
                
            }
            return success;
        }

        /// <summary>
        /// Delete method for the product attribute
        /// </summary>
        /// <param name="productId">id of product to be deleted</param>
        public bool Delete(int productAttributeId)
        {
            var success = false;
            if (productAttributeId > 0)
            {              
                var product = Unitofwork.ProductAttributeLookupRepository.GetByID(productAttributeId);
                if (product != null)
                {
                    Unitofwork.ProductAttributeLookupRepository.Delete(product);
                    Unitofwork.Commit();
                    success = true;
                }
               
            }
            return success;
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        public IEnumerable<ProductAttributeLookup> GetAll()
        {
            var productList = Unitofwork.ProductAttributeLookupRepository.GetAll();
            return productList;
        }

        /// <summary>
        /// get method on the basis of product id
        /// </summary>
        /// <param name="productid">Unieque id of product</param>
        /// <returns>product object</returns>
        public ProductAttributeLookup GetByID(int productid)
        {
            return Unitofwork.ProductAttributeLookupRepository.GetByID(productid);
        }


    }
}
