﻿using System;
using System.Collections.Generic;
using EcommerceDemo.Abstractions;


namespace EcommerceDemo.BAL
{
    public class ProductCategoryService : IProductCategoryService
    {

        IUnitOfWork Unitofwork;

         public ProductCategoryService(IUnitOfWork unitOfWork)
        {
            Unitofwork = unitOfWork;
        }

        /// <summary>
        /// Add product category
        /// </summary>
        /// <param name="entity">product category to be added</param>
        public void Add(ProductCategory entity)
        {
            Unitofwork.ProductCategoryRepository.Add(entity);
            Unitofwork.Commit();
            // _context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Update the product category based on id
        /// </summary>
        /// <param name="categoryId">category id to be updated</param>
        /// <param name="categoryEntity">product category information to be  updated</param>
        /// <returns>true/false</returns>
        public bool Update(int categoryId, ProductCategory categoryEntity)
        {
            var success = false;
            if (categoryEntity != null)
            {
                
                var productCategory = Unitofwork.ProductCategoryRepository.GetByID(categoryId);
                if (productCategory != null)
                {
                    productCategory.CategoryName = categoryEntity.CategoryName;                    
                    Unitofwork.ProductCategoryRepository.Update(productCategory);
                    Unitofwork.Commit();                   
                    success = true;
                }
                
            }
            return success;
        }

        /// <summary>
        /// Delete method for the product category 
        /// </summary>
        /// <param name="categoryId">id of product category to be deleted</param>
        public bool Delete(int categoryId)
        {
            var success = false;
            if (categoryId > 0)
            {              
                var productCategory = Unitofwork.ProductCategoryRepository.GetByID(categoryId);
                if (productCategory != null)
                {
                    Unitofwork.ProductCategoryRepository.Delete(productCategory);
                    Unitofwork.Commit();
                    success = true;
                }
               
            }
            return success;
        }

        /// <summary>
        /// Generic Get All product category
        /// </summary>
        /// <returns>Return all product category</returns>
        public IEnumerable<ProductCategory> GetAll()
        {
            var productList = Unitofwork.ProductCategoryRepository.GetAll();
            return productList;
        }

        /// <summary>
        /// get method on the basis of product category id
        /// </summary>
        /// <param name="categoryid">Unieque id of product category</param>
        /// <returns>product category object</returns>
        public ProductCategory GetByID(int categoryid)
        {
            return Unitofwork.ProductCategoryRepository.GetByID(categoryid);
        }

    }
}
