﻿using System;
using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Linq;

namespace EcommerceDemo.BAL
{
    public class ProductService : IProductService
    {

        IUnitOfWork Unitofwork;

         public ProductService(IUnitOfWork unitOfWork)
        {
            Unitofwork = unitOfWork;
        }

        /// <summary>
        /// Add product
        /// </summary>
        /// <param name="entity">product to be added</param>
        public void Add(Product entity)
        {
            Unitofwork.ProductRepository.Add(entity);
            Unitofwork.Commit();
            // _context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Update the product based on id
        /// </summary>
        /// <param name="productId">product id to be updated</param>
        /// <param name="productEntity">product information to be  updated</param>
        /// <returns>true/false</returns>
       public bool Update(int productId, Product productEntity)
        {
            var success = false;
            if (productEntity != null)
            {
                
                var product = Unitofwork.ProductRepository.GetByID(productId);
                if (product != null)
                {
                    product.ProdName = productEntity.ProdName;
                    product.ProdDescription = productEntity.ProdDescription;
                    product.ProdCatId = productEntity.ProdCatId;
                    Unitofwork.ProductRepository.Update(product);
                    Unitofwork.Commit();                   
                    success = true;
                }
                
            }
            return success;
        }

        /// <summary>
        /// Delete method for the product 
        /// </summary>
        /// <param name="productId">id of product to be deleted</param>
        public bool Delete(int productId)
        {
            var success = false;
            if (productId > 0)
            {              
                var product = Unitofwork.ProductRepository.GetByID(productId);
                if (product != null)
                {
                    Unitofwork.ProductRepository.Delete(product);
                    Unitofwork.Commit();
                    success = true;
                }
               
            }
            return success;
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        public IEnumerable<Product> GetAll()
        {
            var productList = Unitofwork.ProductRepository.GetAll();
            return productList;
        }

        /// <summary>
        /// get method on the basis of product id
        /// </summary>
        /// <param name="productid">Unieque id of product</param>
        /// <returns>product object</returns>
        public Product GetByID(int productid)
        {
            return Unitofwork.ProductRepository.GetByID(productid);
        }


        /// <summary>
        /// Get product listing with pagination support
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="PageSize">Page size</param>
        /// <returns>Product list with pagination info</returns>
        public IProductInfoWithPagination GetProductsWithPaging( int pageIndex = 0,int pageSize = 1)
        {
            int totalRecord = 0;
            var Products = Unitofwork.ProductRepository.GetProductsWithPaging(out totalRecord, pageIndex, pageSize);

         IProductInfoWithPagination p = new ProductInfoWithPagination()
                {
                    Products = Products.ToList(),
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    TotalRecord = totalRecord
                };
                return p;
        }

    }
}
