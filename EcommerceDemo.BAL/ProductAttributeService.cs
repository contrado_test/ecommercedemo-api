﻿using System;
using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Linq;

namespace EcommerceDemo.BAL
{
    public class ProductAttributeService : IProductAttributeService
    {

        IUnitOfWork Unitofwork;

         public ProductAttributeService(IUnitOfWork unitOfWork)
        {
            Unitofwork = unitOfWork;
        }

        /// <summary>
        /// Add product attribute
        /// </summary>
        /// <param name="entity">product attribute to be added</param>
        public void Add(ProductAttribute entity)
        {
            Unitofwork.ProductAttributeRepository.Add(entity);
            Unitofwork.Commit();
            // _context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Update the product attribute based on id
        /// </summary>
        /// <param name="productAttributeId">product attribute id to be updated</param>
        /// <param name="productAttributeEntity">product attribute information to be  updated</param>
        /// <returns>true/false</returns>
        public bool Update(int productAttributeId, ProductAttribute productAttributeEntity)
        {
            var success = false;
            if (productAttributeEntity != null)
            {
                
                var productAttribute = Unitofwork.ProductAttributeRepository.GetByID(productAttributeId);
                if (productAttributeEntity != null)
                {
                    productAttribute.ProductId = productAttributeEntity.ProductId;
                    productAttribute.AttributeValue = productAttributeEntity.AttributeValue;
                 
                    Unitofwork.ProductAttributeRepository.Update(productAttribute);
                    Unitofwork.Commit();                   
                    success = true;
                }
                
            }
            return success;
        }

        /// <summary>
        /// Delete method for the product attribute
        /// </summary>
        /// <param name="productId">id of product to be deleted</param>
        public bool Delete(int productAttributeId)
        {
            var success = false;
            if (productAttributeId > 0)
            {              
                var product = Unitofwork.ProductAttributeRepository.GetByID(productAttributeId);
                if (product != null)
                {
                    Unitofwork.ProductAttributeRepository.Delete(product);
                    Unitofwork.Commit();
                    success = true;
                }
               
            }
            return success;
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        public IEnumerable<ProductAttribute> GetAll()
        {
            var productList = Unitofwork.ProductAttributeRepository.GetAll();
            return productList;
        }

        /// <summary>
        /// get method on the basis of product id
        /// </summary>
        /// <param name="productid">Unieque id of product</param>
        /// <returns>product object</returns>
        public ProductAttribute GetByID(int productid)
        {
            return Unitofwork.ProductAttributeRepository.GetByID(productid);
        }


    }
}
