using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System;

namespace EcommerceDemo.DAL
{ 
    public class ProductAttributeRepository : GenericRepository<ProductAttribute>, IProductAttributeRepository
    {
        private EcommerceDemoEntities EcommerceDemoContext;
        public ProductAttributeRepository(EcommerceDemoEntities context)
            : base(context)
        {
            EcommerceDemoContext = context;
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all entity</returns>
        public override ProductAttribute GetByID(object id)
        {
            var pid = Convert.ToInt32(id);
            var product = from p in EcommerceDemoContext.ProductAttributes.Include("Product")
                            where p.ProductId == pid
                          select p;
            return product.FirstOrDefault<ProductAttribute>();
        }
   
    }
}