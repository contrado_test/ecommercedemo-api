using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System;

namespace EcommerceDemo.DAL
{ 
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private EcommerceDemoEntities EcommerceDemoContext;
        public ProductRepository(EcommerceDemoEntities context)
            : base(context)
        {
            EcommerceDemoContext = context;
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all entity</returns>
        public override Product GetByID(object id)
        {
            var pid = Convert.ToInt32(id);
            var product = from p in EcommerceDemoContext.Products.Include("ProductCategory")
                            where p.ProductId == pid
                          select p;
            return product.FirstOrDefault<Product>();
        }


        /// <summary>
        /// Get product listing with pagination support
        /// </summary>
        /// <param name="totalRecord">Out Parameter total record</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="PageSize">Page size</param>
        /// <returns>List of product object</returns>
        public IEnumerable<GetProductsWithPaginationResult> GetProductsWithPaging(out int totalRecord,int pageIndex = 0,int PageSize = 1)
        {
            
            ObjectParameter recordCount = new ObjectParameter("RecordCount", typeof(int)); 
            var value = EcommerceDemoContext.getProductsWithPagination(pageIndex, PageSize, recordCount).ToList();
            totalRecord = Convert.ToInt32(recordCount.Value);
            return value;
          
        }
   
    }
}