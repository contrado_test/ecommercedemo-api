﻿using EcommerceDemo.Abstractions;


namespace EcommerceDemo.DAL
{
   public class ProductCategoryRepository : GenericRepository<ProductCategory>, IProductCategoryRepository
    {
        private EcommerceDemoEntities storeDBContext;
        public ProductCategoryRepository(EcommerceDemoEntities context)
            : base(context)
        {
            storeDBContext = context;
        }
    }
}
