using System.Collections.Generic;
using EcommerceDemo.Abstractions;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System;

namespace EcommerceDemo.DAL
{ 
    public class ProductAttributeLookupRepository : GenericRepository<ProductAttributeLookup>, IProductAttributeLookupRepository
    {
        private EcommerceDemoEntities EcommerceDemoContext;
        public ProductAttributeLookupRepository(EcommerceDemoEntities context)
            : base(context)
        {
            EcommerceDemoContext = context;
        }


        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all entity</returns>
        public override ProductAttributeLookup GetByID(object id)
        {
            var pid = Convert.ToInt32(id);
            var product = from p in EcommerceDemoContext.ProductAttributeLookups.Include("ProductCategory")
                          where p.AttributeId == pid
                          select p;
            return product.FirstOrDefault<ProductAttributeLookup>();
        }

    }
}