using System.Collections.Generic;
using System.Linq;
using EcommerceDemo.Abstractions;
using System.Data.Entity;

namespace EcommerceDemo.DAL
{
public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
       private readonly EcommerceDemoEntities _context;
        public GenericRepository(EcommerceDemoEntities context)
        {
            _context = context;
        }
        /// <summary>
        /// Genetic Add entity
        /// </summary>
        /// <param name="entity">entity to be added</param>
        public virtual void Add(T entity)
        {
            _context.Set<T>().Add(entity);
        }

        /// <summary>
        /// Generic Delete method for the entities
        /// </summary>      
        /// <param name="entity">entity object to be deleted</param>
        public virtual void Delete(T entity)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
            {
                _context.Set<T>().Attach(entity);
            }
            _context.Set<T>().Remove(entity);
        }

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all entity</returns>
        public virtual IEnumerable<T> GetAll()
        {
            return _context.Set<T>().AsEnumerable<T>();          
        }

        /// <summary>
        /// Generic get method on the basis of id for Entities.
        /// </summary>
        /// <param name="id">Unieque id of entity</param>
        /// <returns>entity object</returns>
        public virtual T GetByID(object id)
        {
            return _context.Set<T>().Find(id);
        }

        /// <summary>
        /// Generic Update entity method
        /// </summary>
        /// <param name="entity">entity object to be updated</param>
        public virtual void Update(T entity)
        {
            _context.Set<T>().Attach(entity);
            _context.Entry(entity).State = EntityState.Modified;
            
        }
    }
}