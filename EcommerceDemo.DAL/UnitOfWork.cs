using EcommerceDemo.Abstractions;
using System;

namespace EcommerceDemo.DAL
{
   public class UnitOfWork :  IUnitOfWork
    {
        public EcommerceDemoEntities Context { get; }
 
        public UnitOfWork()
        {
            Context = new EcommerceDemoEntities();
            Context.Configuration.LazyLoadingEnabled = false;
        }

        
    ProductRepository productRepository = null;
    ProductCategoryRepository productCategoryRepository = null;
    ProductAttributeRepository productAttributeRepository = null;
        ProductAttributeLookupRepository productAttributeLookupRepository = null;


    public IProductRepository ProductRepository
    {
        get
        {
            if (productRepository == null)
            {
                productRepository = new ProductRepository(Context);
            }
            return productRepository;
        }
    }

    
        public IProductCategoryRepository ProductCategoryRepository
        {
            get
            {
                if (productCategoryRepository == null)
                {
                    productCategoryRepository = new ProductCategoryRepository(Context);
                }
                return productCategoryRepository;
            }
        }

        public IProductAttributeRepository ProductAttributeRepository
        {
            get
            {
                if (productAttributeRepository == null)
                {
                    productAttributeRepository = new ProductAttributeRepository(Context);
                }
                return productAttributeRepository;
            }
        }

        public IProductAttributeLookupRepository ProductAttributeLookupRepository
        {
            get
            {
                if (productAttributeLookupRepository == null)
                {
                    productAttributeLookupRepository = new ProductAttributeLookupRepository(Context);
                }
                return productAttributeLookupRepository;
            }
        }


        public void Commit()
        {
            Context.SaveChanges();
        }

        private bool disposed = false;


        /// <summary>
        /// Protected Virtual Dispose method
        /// </summary>
        /// <param name="disposing">true/false</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {                    
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}