﻿using EcommerceDemo.Abstractions;
using System.Collections.Generic;

using System.Web.Http;

namespace EcommerceDemo.Controllers
{
    public class ProductAttributeLookupController : ApiController
    {
        

        IProductAttributeLookupService ProductAttributeLookupService = null;
        public ProductAttributeLookupController(IProductAttributeLookupService productAttributeLookupService)
        {

            ProductAttributeLookupService = productAttributeLookupService;
        }
       
        [Route("api/productAttributeLookup/all")]
        [HttpGet]
        public IEnumerable<ProductAttributeLookup> GetAll()
        {
            return ProductAttributeLookupService.GetAll();
        }
                
        [HttpGet]
        public ProductAttributeLookup GetById(int id)
        {
            return ProductAttributeLookupService.GetByID(id);
        }



        // POST: api/ProductAttributeLookup
        [HttpPost]
        public void Post([FromBody] ProductAttributeLookup p)
        {
            ProductAttributeLookupService.Add(p);
        }

        // PUT: api/ProductAttribute/5
        [HttpPut]
        public bool Put(int id, [FromBody] ProductAttributeLookup p)
        {
            return ProductAttributeLookupService.Update(id,p);
        }

        // DELETE: api/ProductAttributeLookup/5

            [HttpDelete]
        public bool Delete(int id)
        {
          return ProductAttributeLookupService.Delete(id);
        }
    }
}
