﻿using EcommerceDemo.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceDemo.Controllers
{
    public class CategoryController : ApiController
    {
        IProductCategoryService ProductCategoryService = null;
        public CategoryController(IProductCategoryService productCategoryService)
        {

            ProductCategoryService = productCategoryService;
        }

        [Route("api/Category/all")]
        [HttpGet]
        public IEnumerable<ProductCategory> GetAll()
        {
            return ProductCategoryService.GetAll();
        }

        // GET: api/Category/5
        [HttpGet]
        public ProductCategory GetById(int id)
        {
            return ProductCategoryService.GetByID(id);
        }


        // POST: api/Category
        [HttpPost]
        public void Post([FromBody] ProductCategory p)
        {
            ProductCategoryService.Add(p);
        }

        // PUT: api/Category/5
        [HttpPut]
        public bool Put(int id, [FromBody] ProductCategory p)
        {
            return ProductCategoryService.Update(id, p);
        }

        // DELETE: api/Category/5

        [HttpDelete]
        public bool Delete(int id)
        {
            return ProductCategoryService.Delete(id);
        }
    }
}
