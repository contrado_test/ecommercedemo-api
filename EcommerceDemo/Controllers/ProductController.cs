﻿using EcommerceDemo.Abstractions;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EcommerceDemo.Controllers
{
    public class ProductController : ApiController
    {
        

        IProductService ProductService = null;
        public ProductController(IProductService productService)
        {
            
            ProductService = productService;
        }
       
        [Route("api/product/all")]
        [HttpGet]
        public IEnumerable<Product> GetAll()
        {
            return ProductService.GetAll();
        }
                
        [HttpGet]
        public Product GetById(int id)
        {
            return ProductService.GetByID(id);
        }

        // GET: api/{pageIndex}/{pageSize}/Products"
        [Route("api/{pageIndex}/{pageSize}/Products")]
        [HttpGet]
        public IProductInfoWithPagination GetProductsWithPaging(int pageIndex,int pageSize)
        {
            return ProductService.GetProductsWithPaging(pageIndex, pageSize);
        }

        // POST: api/Product
        [HttpPost]
        public HttpResponseMessage Post([FromBody] Product p)
        {
            if(p.ProdCatId <=0)
            {
                string message = string.Format("Please supply product category");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
            }

            ProductService.Add(p);
            return Request.CreateResponse(HttpStatusCode.OK); 
        }

        // PUT: api/Product/5
        [HttpPut]
        public HttpResponseMessage Put(int id, [FromBody] Product p)
        {
            if (p.ProdCatId <= 0)
            {
                string message = string.Format("Please supply product category");
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, message);
            }
            var flag = ProductService.Update(id,p);
            return Request.CreateResponse(HttpStatusCode.OK,flag);
        }

        // DELETE: api/Product/5

            [HttpDelete]
        public bool Delete(int id)
        {           
            return  ProductService.Delete(id);
        }
    }
}
