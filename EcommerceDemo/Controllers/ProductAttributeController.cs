﻿using EcommerceDemo.Abstractions;
using System.Collections.Generic;

using System.Web.Http;

namespace EcommerceDemo.Controllers
{
    public class ProductAttributeController : ApiController
    {
        

        IProductAttributeService ProductAttributeService = null;
        public ProductAttributeController(IProductAttributeService productAttributeService)
        {

            ProductAttributeService = productAttributeService;
        }
       
        [Route("api/productAttribute/all")]
        [HttpGet]
        public IEnumerable<ProductAttribute> GetAll()
        {
            return ProductAttributeService.GetAll();
        }
                
        [HttpGet]
        public ProductAttribute GetById(int id)
        {
            return ProductAttributeService.GetByID(id);
        }

       

        // POST: api/ProductAttribute
        [HttpPost]
        public void Post([FromBody] ProductAttribute p)
        {
            ProductAttributeService.Add(p);
        }

        // PUT: api/ProductAttribute/5
        [HttpPut]
        public bool Put(int id, [FromBody] ProductAttribute p)
        {
            return ProductAttributeService.Update(id,p);
        }

        // DELETE: api/Product/5

            [HttpDelete]
        public bool Delete(int id)
        {
          return ProductAttributeService.Delete(id);
        }
    }
}
