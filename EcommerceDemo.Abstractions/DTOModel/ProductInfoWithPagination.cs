﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceDemo.Abstractions
{
    public class ProductInfoWithPagination : IProductInfoWithPagination
    {
        public List<GetProductsWithPaginationResult> Products { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public int TotalRecord { get; set; }
    }
}
