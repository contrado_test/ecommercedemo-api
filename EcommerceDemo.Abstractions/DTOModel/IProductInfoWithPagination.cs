﻿using System.Collections.Generic;

namespace EcommerceDemo.Abstractions
{
    public interface IProductInfoWithPagination
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
        List<GetProductsWithPaginationResult> Products { get; set; }
        int TotalRecord { get; set; }
    }
}