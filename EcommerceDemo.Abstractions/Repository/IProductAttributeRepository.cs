using System.Collections.Generic;

namespace EcommerceDemo.Abstractions
{
    public interface IProductAttributeRepository : IGenericRepository<ProductAttribute>
    {
        
    }
}