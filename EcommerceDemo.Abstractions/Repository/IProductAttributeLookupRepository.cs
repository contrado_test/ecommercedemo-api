using System.Collections.Generic;

namespace EcommerceDemo.Abstractions
{
    public interface IProductAttributeLookupRepository : IGenericRepository<ProductAttributeLookup>
    {
        
    }
}