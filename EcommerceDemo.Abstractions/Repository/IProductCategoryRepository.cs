using System.Collections.Generic;

namespace EcommerceDemo.Abstractions
{
    public interface IProductCategoryRepository : IGenericRepository<ProductCategory>
    {
      
    }
}