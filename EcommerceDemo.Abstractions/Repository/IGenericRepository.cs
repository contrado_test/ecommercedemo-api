
using System.Collections.Generic;
namespace EcommerceDemo.Abstractions
{
public interface IGenericRepository<T> where T : class
    {
        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all entity</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Generic get method on the basis of id for Entities.
        /// </summary>
        /// <param name="id">Unieque id of entity</param>
        /// <returns>entity object</returns>
        T GetByID(object id);

        /// <summary>
        /// Genetic Add entity
        /// </summary>
        /// <param name="entity">entity to be added</param>
        void Add(T entity);

        /// <summary>
        /// Generic Delete method for the entities
        /// </summary>      
        /// <param name="entity">entity object to be deleted</param>
        void Delete(T entity);

        /// <summary>
        /// Generic Update entity method
        /// </summary>
        /// <param name="entity">entity object to be updated</param>
        void Update(T entity);
      }
}