using System.Collections.Generic;

namespace EcommerceDemo.Abstractions
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        /// <summary>
        /// Get product listing with pagination support
        /// </summary>
        /// <param name="totalRecord">Out Parameter total record</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="PageSize">Page size</param>
        /// <returns>List of product object</returns>
        IEnumerable<GetProductsWithPaginationResult> GetProductsWithPaging(out int totalRecord, int pageIndex = 0, int PageSize = 1);
    }
}