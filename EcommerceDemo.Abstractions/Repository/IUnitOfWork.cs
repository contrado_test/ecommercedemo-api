using System;


namespace EcommerceDemo.Abstractions
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Product Repository
        /// </summary>
         IProductRepository ProductRepository { get; }

        /// <summary>
        /// Product category Repository
        /// </summary>
        IProductCategoryRepository ProductCategoryRepository { get; }

        /// <summary>
        /// Product attribute Repository
        /// </summary>
        IProductAttributeRepository ProductAttributeRepository { get; }

        /// <summary>
        /// Product attribute Repository
        /// </summary>
        IProductAttributeLookupRepository ProductAttributeLookupRepository { get; }
        void Commit();
    }
}