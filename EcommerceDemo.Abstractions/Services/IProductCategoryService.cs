﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceDemo.Abstractions
{
    public interface IProductCategoryService
    {
        /// <summary>
        /// get method on the basis of product category id
        /// </summary>
        /// <param name="categoryid">Unieque id of product category</param>
        /// <returns>product object</returns>
        ProductCategory GetByID(int categoryid);

        /// <summary>
        /// Add product category
        /// </summary>
        /// <param name="entity">product to be added</param>
        void Add(ProductCategory entity);

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product category</returns>
        IEnumerable<ProductCategory> GetAll();

        /// <summary>
        /// Delete method for the product  category
        /// </summary>
        /// <param name="categoryId">id of product category to be deleted</param>
        bool Delete(int categoryId);

        /// <summary>
        /// Update the product based on id
        /// </summary>
        /// <param name="categoryId">product category id to be updated</param>
        /// <param name="entity">product  category information to be  updated</param>
        /// <returns></returns>
        bool Update(int categoryId, ProductCategory entity);       

    }

}
