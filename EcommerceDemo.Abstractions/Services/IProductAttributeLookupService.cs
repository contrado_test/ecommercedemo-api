﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceDemo.Abstractions
{
    public interface IProductAttributeLookupService
    {
        /// <summary>
        /// get method on the basis of product attribute lookup id
        /// </summary>
        /// <param name="productattributeid">Unieque id of product attribute lookup</param>
        /// <returns>product attribute lookup object</returns>
        ProductAttributeLookup GetByID(int productattributelookupid);

        /// <summary>
        /// Add product attribute
        /// </summary>
        /// <param name="entity">product attribute to be added</param>
        void Add(ProductAttributeLookup entity);

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        IEnumerable<ProductAttributeLookup> GetAll();

        /// <summary>
        /// Delete method for the product  attribute lookup
        /// </summary>
        /// <param name="productAttributeLookupId">id of product Attribute to be deleted</param>
        bool Delete(int productAttributeLookupId);

        /// <summary>
        /// Update the product attribute lookup based on id
        /// </summary>
        /// <param name="productAttributeLookupId">product Attribute lookup id to be updated</param>
        /// <param name="productAttributeLookupEntity">product attribute lookup information to be  updated</param>
        /// <returns>true/false</returns>
        bool Update(int productAttributeLookupId, ProductAttributeLookup productAttributeLookupEntity);

    }

}
