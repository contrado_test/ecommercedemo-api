﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceDemo.Abstractions
{
    public interface IProductService
    {
        /// <summary>
        /// get method on the basis of product id
        /// </summary>
        /// <param name="productid">Unieque id of product</param>
        /// <returns>product object</returns>
        Product GetByID(int productid);

        /// <summary>
        /// Add product
        /// </summary>
        /// <param name="entity">product to be added</param>
        void Add(Product entity);

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        IEnumerable<Product> GetAll();

        /// <summary>
        /// Delete method for the product 
        /// </summary>
        /// <param name="productId">id of product to be deleted</param>
        bool Delete(int productId);

        /// <summary>
        /// Update the product based on id
        /// </summary>
        /// <param name="productId">product id to be updated</param>
        /// <param name="productEntity">product information to be  updated</param>
        /// <returns></returns>
        bool Update(int productId, Product productEntity);

        /// <summary>
        /// Get product listing with pagination support
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="PageSize">Page size</param>
        /// <returns>List of product object</returns>
        IProductInfoWithPagination GetProductsWithPaging(int pageIndex = 0, int PageSize = 1);


    }

}
