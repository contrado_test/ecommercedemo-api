﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceDemo.Abstractions
{
    public interface IProductAttributeService
    {
        /// <summary>
        /// get method on the basis of product attribute id
        /// </summary>
        /// <param name="productattributeid">Unieque id of product attribute</param>
        /// <returns>product object</returns>
        ProductAttribute GetByID(int productattributeid);

        /// <summary>
        /// Add product attribute
        /// </summary>
        /// <param name="entity">product attribute to be added</param>
        void Add(ProductAttribute entity);

        /// <summary>
        /// Generic Get All Entity
        /// </summary>
        /// <returns>Return all product</returns>
        IEnumerable<ProductAttribute> GetAll();

        /// <summary>
        /// Delete method for the product 
        /// </summary>
        /// <param name="productAttributeId">id of product Attribute to be deleted</param>
        bool Delete(int productAttributeId);

        /// <summary>
        /// Update the product attribute based on id
        /// </summary>
        /// <param name="productAttributeId">product Attribute id to be updated</param>
        /// <param name="productEntity">product information to be  updated</param>
        /// <returns></returns>
        bool Update(int productId, ProductAttribute productEntity);

    }

}
